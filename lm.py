import nltk
from nltk.util import pad_sequence
from nltk.util import bigrams
from nltk.util import ngrams
from nltk.util import everygrams
from nltk.lm.preprocessing import pad_both_ends, padded_everygram_pipeline
from nltk.lm.preprocessing import flatten
from nltk.lm import MLE, Laplace

import pandas as pd
import loadutils

# Toy dataset
# No need for tokenization
tokenized_texts = [
        ['I', 'am', 'Sam'],
        ['Sam', 'am', 'I'],
        ['I', 'do', 'not', 'like', 'green', 'eggs', 'and', 'ham'] ]

# load real data from a file and tokenize them
tokenized_texts = loadutils.load('C:/Users/galanisd/Desktop/20_21LangTech/sentiment-analysis-on-movie-reviews/__train.tsv')

# Already done these in lecture 02 (Ngrams.py)
# print bigrams
print('bigrams')
print(list(nltk.bigrams(tokenized_texts[2])))
# print trigrams
print('trigrams')
print(list(nltk.trigrams(tokenized_texts[2])))

# print 4-grams
print('4-grams')
print(list(ngrams(tokenized_texts[2], n=4)))

# pad, i.e add start/end symbols
padded = list(pad_sequence(tokenized_texts[2], pad_left=True, left_pad_symbol="<s>", pad_right=True, right_pad_symbol="</s>", n=2))
print('padded sentence #3')
print(padded)

# same as above but without passing all these parameters
padded = list(pad_both_ends(tokenized_texts[2], n=2))
print('padded sentence #3')
print(padded)


#
# For training an LM we need  "one flat stream of words."
# E.g.
print("flat stream")
flat = list(flatten(pad_both_ends(sent, n=2) for sent in tokenized_texts))
#print(flat)

# --------------------------------------------------------------
# --------------------------------------------------------------

# Above we have learnt how to preprocess our input
# in order to train an LM model
# However nltk provides  padded_everygram_pipeline
# that does everything for us. No need to write the code again
n = 2
# n -> the order of the LM
# order -> Largest ngram length produced

# Creates two iterators:
# sentences padded and turned into sequences of nltk.util.everygrams
# sentences padded as above and chained together for a flat stream of words

padded_sentences_for_training, flat_stream_for_vocabulary = padded_everygram_pipeline(n, tokenized_texts)

#print()
#print('Padded sentences')
#print('#############')
#for padded_sent in padded_sentences_for_training:
#    print('padded sentence')
#    print(list(padded_sent))
#print('#############')

# Initialize LM trainer
lm = MLE(n)
lm = Laplace(n)
print('vocabulary size before train')
print(len(lm.vocab))

# Train model
lm.fit(text=padded_sentences_for_training, vocabulary_text=flat_stream_for_vocabulary)

# Vocabulary after train
print('vocabulary size after train')
print(len(lm.vocab))
#print('vocabulary description')
print(lm.vocab.counts)

# Lookup words of the sentence #1
print(lm.vocab.lookup(tokenized_texts[0]))

# Lookup for unseen sentence
# it automatically replace words not in the vocabulary with `<UNK>`.
print(lm.vocab.lookup(' I will eat ham .'.split()))

# Print counts
print('Counts')
print(lm.counts)

# Print scores, probabilities for specific cases

# counts: Returns counts
# score: Masks out of vocab (OOV) words and computes their model score.

def info(word):

        print("word:" + word)
        print('---')
        print(word)
        print(lm.counts[word])
        print(lm.score(word))
        print(lm.logscore(word))
        print('---\n\n')


info('am')
info('movie')
info('good')


print('movie|good')
print(sorted(lm.counts[['good']].items()))
print(lm.counts[['good']]['movie'])
print(lm.score('movie', ['good']))
print(lm.logscore('movie', ['good']))
print()

# director is a magician -> exists in the training set
print('magician')
print(lm.score('magician'))
print('magician|a')
print(lm.score('magician', 'a'.split()))
print('magician|is a')
print(lm.score('magician', 'is a'.split()))
print('magician|director is a')
print(lm.score('magician', 'director is a'.split()))
print('genius|director is a')
print(lm.score('genius', 'director is a'.split()))
print('refrigerator|director is a')
print(lm.score('refrigerator', 'director is a'.split()))
