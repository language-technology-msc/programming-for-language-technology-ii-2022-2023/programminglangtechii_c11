import nltk
import pandas as pd

def load(file):
    docs = pd.read_csv(file, sep='\t')
    # Load the specific tsv column
    phrases = docs['Phrase']

    tokenized_texts = [list(map(str.lower, nltk.word_tokenize(phrase))) for phrase in phrases]
    return tokenized_texts

