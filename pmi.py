# import nltk
import nltk

# We will use reuters corpus that is included in nltk
# It contains news articles.
from nltk.corpus import reuters

# BigramAssocMeasures implements/provides several bigram association measures.
# Among them PMI
from nltk.metrics import BigramAssocMeasures

# Load all reuters coprus words to an array
words = nltk.corpus.reuters.words()

# Print total num of words
print('Num of words')
print(len(words))

# Construct a BigramCollocationFinder for all bigrams in the given sequence (words)
# A tool for the finding and ranking of bigram collocations
# Ranking is done based on some funtion
finder = nltk.BigramCollocationFinder.from_words(words)

# Same as above but with custom window_size
#win_size = 3
#finder = nltk.BigramCollocationFinder.from_words(words, win_size)

# Keeps only bigrams that appear x+ times
finder.apply_freq_filter(10)

# Returns the top ngrams when scored by the given function
# ... which in this case is pmi
scores = finder.nbest(BigramAssocMeasures.pmi, 50)

print('---------------')
# print results
for score in scores[:30]:
    print(score)

# "Returns a sequence of (ngram, score) pairs ordered from highest to
# lowest score, as determined by the scoring function provided
scores = finder.score_ngrams(BigramAssocMeasures.pmi)

print('---------------')
# print results
for score in scores[:30]:
    print(score)

print('collocation (in ... end)')
print(finder.score_ngram(BigramAssocMeasures.pmi, 'in' , 'end'))
# None when window_size = 2
# -0.772189725324175 when window_size = 5
# -0.3333054840909675 when window_size = 10
# -0.11322664315924769 when window_size = 20

print('collocation (United ... America)')
print(finder.score_ngram(BigramAssocMeasures.pmi, 'United' , 'America'))
# None when window_size = 2
# None when window_size = 3
# -2.357152226045333 when window_size = 4